use Mix.Config

config :derivco, Derivco.League,
  leagues: [
    {:la_league, filepath: "files/test_data.csv"}
  ]
