use Mix.Config

config :derivco, Derivco.League,
  leagues: [
    {:la_league, filepath: "files/data.csv"}
  ]
