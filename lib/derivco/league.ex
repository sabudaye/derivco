defmodule Derivco.League do
  @moduledoc """
  League data repository, each league starts as separeted process
  """

  use Agent
  alias Derivco.Match
  alias Derivco.CsvData
  require Logger

  def start_link(state) do
    Logger.debug(fn -> "Start League with #{inspect(state)}" end)
    matches = get_matches_from_file(Keyword.get(state, :filepath))
    Agent.start_link(fn -> matches end, state)
  end

  @spec get_matches(atom, any(), any()) :: {:ok, list()}
  @doc """
  Get matches by params
  """
  def get_matches(league_name, nil, nil), do: get_all_matches(league_name)
  def get_matches(league_name, division, nil), do: get_matches_by_division(league_name, division)

  def get_matches(league_name, division, season) do
    get_matches_by_division_and_season(league_name, division, season)
  end

  @spec get_all_matches(atom()) :: {:ok, list()}
  @doc """
  Get all matches
  """
  def get_all_matches(league_name) do
    matches =
      Agent.get(league_name, fn state ->
        state
      end)

    {:ok, matches}
  end

  @spec get_matches_by_division(atom(), String.t()) :: {:ok, list()}
  @doc """
  Get matches by division
  """
  def get_matches_by_division(league_name, division) do
    matches =
      Agent.get(league_name, fn state ->
        Enum.filter(state, fn match -> match.div == division end)
      end)

    {:ok, matches}
  end

  @spec get_matches_by_division_and_season(atom(), String.t(), String.t()) :: {:ok, list()}
  @doc """
  Get matches by division and season
  """
  def get_matches_by_division_and_season(league_name, division, season) do
    matches =
      Agent.get(league_name, fn state ->
        Enum.filter(state, fn match -> match.div == division && match.season == season end)
      end)

    {:ok, matches}
  end

  @spec store_match(atom(), %Match{}) :: :ok
  @doc """
  Store match in league repository
  """
  def store_match(league_name, match) do
    Agent.update(league_name, fn state -> [match | state] end)
  end

  @spec get_matches_from_file(String.t()) :: [Match.t()]
  defp get_matches_from_file(filepath) do
    filepath
    |> CsvData.parse_file()
    |> Enum.map(&Match.create/1)
  end
end
