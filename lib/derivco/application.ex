defmodule Derivco.Application do
  @moduledoc false
  use Application
  use Supervisor

  @spec start(term, term) :: {:error, term} | {:ok, pid} | {:ok, pid, term}
  def start(_type, _args) do
    children =
      [
        Plug.Adapters.Cowboy2.child_spec(
          scheme: :http,
          plug: Derivco.HttpEndpoint,
          options: [
            port: Application.get_env(:derivco, Derivco.HttpEndpoint) |> Keyword.get(:port, 8080)
          ]
        ),
        supervisor(GRPC.Server.Supervisor, [{Derivco.GrpcEndpoint, 50_051}])
      ] ++ league_children()

    opts = [strategy: :one_for_one, name: Derivco.Supervisor]
    Supervisor.start_link(children, opts)
  end

  defp league_children() do
    Application.get_env(:derivco, Derivco.League)
    |> Keyword.get(:leagues, [])
    |> Enum.map(&league_childspec/1)
  end

  defp league_childspec({league_name, league_config}) do
    Derivco.League.child_spec(league_config |> Keyword.put_new(:name, league_name))
  end
end
