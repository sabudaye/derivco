defmodule Derivco.Match do
  @moduledoc """
  Match context: Struct definition, context functions

  FTHG = Full Time Home Team Goals
  FTAG = Full Time Away Team Goals
  FTR = Full Time Result (H=Home Win, D=Draw, A=Away Win)
  HTHG = Half Time Home Team Goals
  HTAG = Half Time Away Team Goals
  HTR = Half Time Result (H=Home Win, D=Draw, A=Away Win)

  div = division
  """
  alias Derivco.Match
  use Protobuf, syntax: :proto3

  @enforce_keys [
    :id,
    :div,
    :season,
    :date,
    :home_team,
    :away_team,
    :fthg,
    :ftag,
    :ftr,
    :hthg,
    :htag,
    :htr
  ]

  defstruct [
    :id,
    :div,
    :season,
    :date,
    :home_team,
    :away_team,
    :fthg,
    :ftag,
    :ftr,
    :hthg,
    :htag,
    :htr
  ]

  @type result :: <<_::1>>

  @type t :: %Derivco.Match{
          id: number(),
          div: String.t(),
          season: String.t(),
          date: String.t(),
          home_team: String.t(),
          away_team: String.t(),
          fthg: number(),
          ftag: number(),
          ftr: result,
          hthg: number(),
          htag: number(),
          htr: result
        }

  field(:id, 1, type: :int32)
  field(:div, 2, type: :string)
  field(:season, 3, type: :string)
  field(:date, 4, type: :string)
  field(:home_team, 5, type: :string)
  field(:away_team, 6, type: :string)
  field(:fthg, 7, type: :int32)
  field(:ftag, 8, type: :int32)
  field(:ftr, 9, type: :string)
  field(:hthg, 10, type: :int32)
  field(:htag, 11, type: :int32)
  field(:htr, 12, type: :string)

  @spec create(Map.t()) :: %Match{}
  @doc """
  create %Match{} struct from map

  ## Examples

      iex> Derivco.Match.create(%{
      ...> "" => "7",
      ...> "AwayTeam" => "Real Madrid",
      ...> "Date" => "21/08/16",
      ...> "Div" => "SP1",
      ...> "FTAG" => "3",
      ...> "FTHG" => "0",
      ...> "FTR" => "A",
      ...> "HTAG" => "2",
      ...> "HTHG" => "0",
      ...> "HTR" => "A",
      ...> "HomeTeam" => "Sociedad",
      ...> "Season" => "201617"
      ...> })
      %Derivco.Match{
        away_team: "Real Madrid",
        date: "21/08/16",
        div: "SP1",
        ftag: 3,
        fthg: 0,
        ftr: "A",
        home_team: "Sociedad",
        htag: 2,
        hthg: 0,
        htr: "A",
        id: 7,
        season: "201617"
      }

  """
  def create(match_data) do
    match_data
    |> camel_keys_to_atoms
    |> set_id
    |> parse_numbers
    |> to_struct
  end

  @spec camel_keys_to_atoms(Map.t()) :: Map.t()
  defp camel_keys_to_atoms(map) do
    Map.new(
      map,
      fn {k, v} ->
        {String.to_atom(Macro.underscore(k)), v}
      end
    )
  end

  @spec set_id(Map.t()) :: Map.t()
  defp set_id(map) do
    Map.merge(map, %{id: map[:""]})
  end

  @spec parse_numbers(Map.t()) :: Map.t()
  defp parse_numbers(map) do
    Map.new(
      map,
      fn {k, v} ->
        {k, to_integer(k, v)}
      end
    )
  end

  @spec parse_numbers(Map.t()) :: Match.t()
  defp to_struct(map) do
    struct(Match, map)
  end

  @spec to_integer(atom(), String.t()) :: String.t() | number()
  defp to_integer(:ftag, value), do: String.to_integer(value)
  defp to_integer(:fthg, value), do: String.to_integer(value)
  defp to_integer(:htag, value), do: String.to_integer(value)
  defp to_integer(:hthg, value), do: String.to_integer(value)
  defp to_integer(:id, value), do: String.to_integer(value)
  defp to_integer(_, value), do: value
end
