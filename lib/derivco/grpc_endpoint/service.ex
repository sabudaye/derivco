defmodule Derivco.GrpcEndpoint.Service do
  @moduledoc """
  gRPC Service declaration
  """
  use GRPC.Service, name: "derivco.GrpcEndpoint"

  rpc(:ListMatches, Derivco.GrpcEndpoint.MatchesRequest, stream(Derivco.Match))
end

defmodule Derivco.GrpcEndpoint.Stub do
  @moduledoc """
  gRPC Service Stub for testing
  """
  use GRPC.Stub, service: Derivco.GrpcEndpoint.Service
end
