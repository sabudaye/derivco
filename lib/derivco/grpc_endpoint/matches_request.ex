defmodule Derivco.GrpcEndpoint.MatchesRequest do
  @moduledoc """
  Protobuf MatchesRequest structure
  """
  use Protobuf

  @type t :: %__MODULE__{
          league_name: String.t(),
          division: String.t(),
          season: String.t()
        }
  defstruct [:league_name, :division, :season]

  field(:league_name, 1, type: :string)
  field(:division, 2, optional: true, type: :string)
  field(:season, 3, optional: true, type: :string)
end
