defmodule Derivco.HttpEndpoint do
  @moduledoc """
  Application HTTP endpoint.

  ## Information about matches.

  ### Get matches in league

  ```
  GET /matches?league={league}&division={division}&season={season}
  ```
  returns list of matches filtered by request parameters
  """

  use Plug.Router
  require Logger

  alias Derivco.League

  plug(Plug.RequestId)

  plug(Plug.Logger)
  plug(:match)
  plug(:dispatch)

  # credo:disable-for-next-line Credo.Check.Readability.Specs
  def init(options), do: options

  def start_link do
    {:ok, _} = Plug.Adapters.Cowboy2.http(__MODULE__, [])
  end

  get "/matches" do
    params = Plug.Conn.fetch_query_params(conn).params
    Logger.info(fn -> "Requested with #{inspect(params)}" end)

    case params["league"] do
      nil ->
        send_resp(conn, 400, "league param is required")

      league ->
        try do
          with {:ok, matches} <-
                 League.get_matches(
                   String.to_existing_atom(league),
                   params["division"],
                   params["season"]
                 ) do
            send_resp(conn, 200, Poison.encode!(%{matches: matches}))
          else
            {:error, reason} ->
              send_resp(conn, 404, "Error: #{reason}")
          end
        rescue
          _error ->
            send_resp(conn, 404, "Error: legeue does not exist")
        end
    end
  end

  match _ do
    send_resp(conn, 404, "")
  end
end
