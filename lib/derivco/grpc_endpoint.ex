defmodule Derivco.GrpcEndpoint do
  @moduledoc """
  Application gRPC endpoint.

  ## Information about matches.

  ### Get all matches in league

  ```
  gRPC list_matches
  ```
  returns list of matches in given league
  """
  use GRPC.Server, service: Derivco.GrpcEndpoint.Service

  require Logger

  @spec list_matches(Derivco.GrpcEndpoint.MatchesReuqesst.t(), GRPC.Server.Stream.t()) :: any
  def list_matches(
        %{league_name: league_name, division: division, season: season} = league,
        stream
      ) do
    Logger.info(fn -> "GRPC list_matches request given with #{inspect(league)}" end)

    {:ok, matches} =
      Derivco.League.get_matches(String.to_existing_atom(league_name), division, season)

    matches
    |> Enum.each(fn match -> GRPC.Server.send_reply(stream, match) end)
  end
end
