defmodule Derivco.CsvData do
  @moduledoc """
  Module for working with data from csv files
  """

  @csv_separator ?,

  @spec parse_file(String.t()) :: [Enum.t()]
  @doc """
  Parse ligue csv file by given path to list of maps
  """
  def parse_file(path) do
    path
    |> File.stream!()
    |> CSV.decode(separator: @csv_separator, headers: true)
    |> Stream.map(&to_map/1)
    |> Enum.to_list()
  end

  defp to_map({:ok, map}), do: map
end
