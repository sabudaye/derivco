defmodule Derivco.GrpcEndpointTest do
  use ExUnit.Case, async: true

  @grpc_port 50_051

  test "returns all matches in league" do
    league = Derivco.GrpcEndpoint.MatchesRequest.new(league_name: "la_league")
    {:ok, ch} = GRPC.Stub.connect("127.0.0.1", @grpc_port, [])
    {:ok, reply_stream} = Derivco.GrpcEndpoint.Stub.list_matches(ch, league)

    result =
      Derivco.Match.new(
        away_team: "Eibar",
        date: "19/08/16",
        div: "SP1",
        ftag: 1,
        fthg: 2,
        ftr: "H",
        home_team: "La Coruna",
        htag: 0,
        hthg: 0,
        htr: "D",
        id: 1,
        season: "201617"
      )

    {:ok, ^result} = reply_stream |> Stream.take(1) |> Enum.to_list() |> List.first()
  end

  test "returns matches by division and season" do
    league =
      Derivco.GrpcEndpoint.MatchesRequest.new(
        league_name: "la_league",
        division: "SP2",
        season: "201617"
      )

    {:ok, ch} = GRPC.Stub.connect("127.0.0.1", @grpc_port, [])
    {:ok, reply_stream} = Derivco.GrpcEndpoint.Stub.list_matches(ch, league)

    result =
      Derivco.Match.new(
        away_team: "Espanol",
        date: "20/08/16",
        div: "SP2",
        ftag: 4,
        fthg: 6,
        ftr: "H",
        home_team: "Sevilla",
        htag: 3,
        hthg: 3,
        htr: "D",
        id: 5,
        season: "201617"
      )

    {:ok, ^result} = reply_stream |> Stream.take(1) |> Enum.to_list() |> List.first()
  end
end
