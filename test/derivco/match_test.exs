defmodule Derivco.MatchTest do
  use ExUnit.Case
  alias Derivco.Match

  doctest Match

  test "encode to protobuf" do
    match = %Match{
      ftag: 1,
      fthg: 2,
      ftr: "H",
      htag: 0,
      hthg: 0,
      htr: "D",
      away_team: "Ath Bilbao",
      date: "21/08/16",
      div: "SP4",
      home_team: "Sp Gijon",
      id: 200,
      season: "201718"
    }

    encoded_match = Match.encode(match)

    assert byte_size(encoded_match) == 58
    assert Match.decode(encoded_match) === match
  end
end
