defmodule Derivco.HttpEndpointTest do
  use ExUnit.Case
  use Plug.Test

  alias Derivco.HttpEndpoint, as: Endpoint

  @opts Endpoint.init([])

  @spec matches_url(map) :: String.t()
  def matches_url(params) do
    query_params =
      [:league, :division, :season]
      |> Enum.map(fn param ->
        case Keyword.get(params, param) do
          nil -> nil
          value -> "#{param}=#{value}"
        end
      end)
      |> Enum.reject(&is_nil/1)
      |> Enum.join("&")

    "/matches?#{query_params}"
  end

  test "returns 404 on wrong resource call" do
    conn = conn(:get, "/foobar") |> Endpoint.call(@opts)
    assert {404, _, _} = sent_resp(conn)
  end

  describe "/matches" do
    test "returns 404 when league does not exist" do
      conn = conn(:get, matches_url(league: "fpl")) |> Endpoint.call(@opts)
      assert {404, _, _} = sent_resp(conn)
    end

    test "returns matches by league" do
      conn = conn(:get, matches_url(league: "la_league")) |> Endpoint.call(@opts)
      assert {200, _, _} = sent_resp(conn)
    end

    test "returns matches by league and division" do
      conn = conn(:get, matches_url(league: "la_league", division: "SP1")) |> Endpoint.call(@opts)
      assert {200, _, _} = sent_resp(conn)
    end

    test "returns matches by league, division and season" do
      conn =
        conn(:get, matches_url(league: "la_league", division: "SP1", season: "201617"))
        |> Endpoint.call(@opts)

      assert {200, _, resp} = sent_resp(conn)
      parsed_resp = Poison.Parser.parse!(resp)

      assert parsed_resp == %{
               "matches" => [
                 %{
                   "away_team" => "Eibar",
                   "date" => "19/08/16",
                   "div" => "SP1",
                   "ftag" => 1,
                   "fthg" => 2,
                   "ftr" => "H",
                   "home_team" => "La Coruna",
                   "htag" => 0,
                   "hthg" => 0,
                   "htr" => "D",
                   "id" => 1,
                   "season" => "201617"
                 },
                 %{
                   "away_team" => "Osasuna",
                   "date" => "19/08/16",
                   "div" => "SP1",
                   "ftag" => 1,
                   "fthg" => 1,
                   "ftr" => "D",
                   "home_team" => "Malaga",
                   "htag" => 0,
                   "hthg" => 0,
                   "htr" => "D",
                   "id" => 2,
                   "season" => "201617"
                 }
               ]
             }
    end
  end
end
