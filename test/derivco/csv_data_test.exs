defmodule Derivco.CsvDataTest do
  use ExUnit.Case
  alias Derivco.CsvData

  doctest CsvData

  test "returns list of maps from given data.csv file path" do
    result =
      "files/test_data.csv"
      |> CsvData.parse_file()

    assert Enum.count(result) == 8
  end
end
