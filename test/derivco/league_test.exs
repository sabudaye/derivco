defmodule Derivco.LeagueTest do
  use ExUnit.Case

  alias Derivco.League

  test "gets matches by division from league repo" do
    {:ok, matches} = League.get_matches_by_division(:la_league, "SP1")

    assert Enum.count(matches) == 4
  end

  test "gets matches by division and season from league repo" do
    {:ok, matches} = League.get_matches_by_division_and_season(:la_league, "SP3", "201718")

    assert Enum.count(matches) == 1

    assert Enum.take(matches, 1) === [
             %Derivco.Match{
               ftag: 1,
               fthg: 2,
               ftr: "H",
               htag: 0,
               hthg: 0,
               htr: "D",
               away_team: "Ath Bilbao",
               date: "21/08/16",
               div: "SP3",
               home_team: "Sp Gijon",
               id: 8,
               season: "201718"
             }
           ]
  end

  test "stores match to league repo" do
    {:ok, matches1} = League.get_all_matches(:la_league)
    assert Enum.count(matches1) == 8

    match = %Derivco.Match{
      ftag: 1,
      fthg: 2,
      ftr: "H",
      htag: 0,
      hthg: 0,
      htr: "D",
      away_team: "Ath Bilbao",
      date: "21/08/16",
      div: "SP4",
      home_team: "Sp Gijon",
      id: 200,
      season: "201718"
    }

    :ok = League.store_match(:la_league, match)
    {:ok, matches2} = League.get_all_matches(:la_league)

    assert Enum.count(matches2) == 9
  end
end
