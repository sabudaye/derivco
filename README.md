## Development

### Local

If you want to run service in local machine, you need:

1.  Install elixir language with version according `.tool-versions` file
3.  Install protobuf-devel package `sudo dnf install protobuf-devel`

```sh
# fetch dependencies
mix deps.get

# run console
iex -S mix

# run application
mix run --no-halt

# run tests
mix test

# check codestyle
mix credo

# check typespecs
mix dialyzer

# check format
mix format --check-formatted
```

### Docker

```sh
# prepate images
docker-compose build
docker-compose run --rm app mix deps.get

# run
docker-compose up app

# run tests
docker-compose run --rm test
```