defmodule Derivco.MixProject do
  use Mix.Project

  def project do
    [
      app: :derivco,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      test_coverage: [tool: ExCoveralls]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :grpc],
      mod: {Derivco.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:csv, "~> 2.0"},
      {:plug, "~> 1.4"},
      {:credo, "~> 0.9.2", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.0.0-rc.2", only: [:dev], runtime: false},
      {:excoveralls, "~> 0.8", only: :test},
      {:ex_doc, "~> 0.16", only: :dev, runtime: false},
      {:protobuf, "~> 0.5.3"},
      {:grpc, github: "tony612/grpc-elixir"},
      {:cowboy, github: "sabudaye/cowboy", override: true, ref: "http2-cancel-timeout"},
      {:cowlib, "~> 2.4.0", override: true}
    ]
  end
end
