FROM elixir:1.6.6

ENV LC_ALL=en_US.utf8
RUN mix local.hex --force
RUN mix local.rebar --force

EXPOSE 8080
EXPOSE 50051
